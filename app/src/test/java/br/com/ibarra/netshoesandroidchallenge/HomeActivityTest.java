package br.com.ibarra.netshoesandroidchallenge;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Map;

/**
 * Created by joaoibarra on 22/12/15.
 */

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class HomeActivityTest{
    private HomeActivity homeActivity;
    private RecyclerView productsList ;
    private LinearLayout progressbarLayout;
    private RelativeLayout errorLayout;
    private Button reloadButton;

    @Before
    public void setUp(){
        homeActivity = Robolectric.buildActivity(HomeActivity.class)
                .create().get();

        productsList = (RecyclerView) homeActivity.findViewById(R.id.products);
        progressbarLayout = (LinearLayout) homeActivity.findViewById(R.id.progressbar);
        reloadButton = (Button) homeActivity.findViewById(R.id.reload);
        progressbarLayout = (LinearLayout) homeActivity.findViewById(R.id.progressbar);
        errorLayout = (RelativeLayout) homeActivity.findViewById(R.id.error);
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull(homeActivity);
    }

    @Test
    public void checkViewElementsNotNull() throws Exception{
        assertNotNull(productsList);
        assertNotNull(progressbarLayout);
        assertNotNull(reloadButton);
        assertNotNull(progressbarLayout);
        assertNotNull(errorLayout);
    }

    @Test
    public void testReloadButtonClick() throws Exception {
        reloadButton.performClick();
        //Se clica em tentar carregar novamente, o progressbar fica visivel
        assertTrue(progressbarLayout.getVisibility() == View.VISIBLE);
    }

}
