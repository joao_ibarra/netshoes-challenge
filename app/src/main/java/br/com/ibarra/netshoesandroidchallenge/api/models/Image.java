package br.com.ibarra.netshoesandroidchallenge.api.models;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class Image {
    private String zoom;
    private String normal;
    private String small;
    private String thumb;
    private String large;
    private String medium;

    private String axis;
    private String height;
    private String frames;
    private String width;
    private String url;


    public String getZoom() {
        return zoom;
    }

    public String getNormal() {
        return normal;
    }

    public String getSmall() {
        return small;
    }

    public String getThumb() {
        return thumb;
    }

    public String getLarge() {
        return large;
    }

    public String getMedium() {
        return medium;
    }
}
