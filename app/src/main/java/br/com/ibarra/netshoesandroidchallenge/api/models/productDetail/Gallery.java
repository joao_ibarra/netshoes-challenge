package br.com.ibarra.netshoesandroidchallenge.api.models.productDetail;

import java.util.List;

import br.com.ibarra.netshoesandroidchallenge.api.models.Image;

/**
 * Created by joaoibarra on 20/12/15.
 */
public class Gallery {
    private List<Image> items;

    public List<Image> getItems() {
        return items;
    }
}
