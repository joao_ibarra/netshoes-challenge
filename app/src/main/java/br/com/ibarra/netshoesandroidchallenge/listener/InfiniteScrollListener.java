package br.com.ibarra.netshoesandroidchallenge.listener;

/**
 * Created by joaoibarra on 19/12/15.
 */
public interface InfiniteScrollListener {

    int loadingRunning = 1;
    int loadingIdle = 0;
    int loadingError = 2;

    void checkDataToAdd();

    void setLoadingStart();

    void setLoadingEnd();

    void setLoadingError();

}
