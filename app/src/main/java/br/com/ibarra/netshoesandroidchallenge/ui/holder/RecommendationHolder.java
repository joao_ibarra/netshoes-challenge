package br.com.ibarra.netshoesandroidchallenge.ui.holder;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.ibarra.netshoesandroidchallenge.R;
import br.com.ibarra.netshoesandroidchallenge.api.models.Product;
import br.com.ibarra.netshoesandroidchallenge.listener.EndlessRecyclerOnScrollListener;
import br.com.ibarra.netshoesandroidchallenge.ui.activities.ProductDetailActivity;
import br.com.ibarra.netshoesandroidchallenge.ui.adapter.ProductAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by joaoibarra on 19/12/15.
 */

public class RecommendationHolder extends RecyclerView.ViewHolder{
    @Bind(R.id.title) TextView title;
    @Bind(R.id.recommendations) RecyclerView recommendationsRecyclerView;
    private List<Product> products;
    ProductAdapter productAdapter;

    public RecommendationHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public RecyclerView getRecommendationsRecyclerView() {
        return recommendationsRecyclerView;
    }

    public void setRecommendationsRecyclerView(RecyclerView recommendationsRecyclerView) {
        this.recommendationsRecyclerView = recommendationsRecyclerView;
    }

    public List<Product> getProduct() {
        return products;
    }

    public void setProduct(List<Product> product) {
        this.products = product;
    }
}
