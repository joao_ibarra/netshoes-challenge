package br.com.ibarra.netshoesandroidchallenge.api.models.productDetail;

/**
 * Created by joaoibarra on 20/12/15.
 */
public class Badge {
    private String strength;
    private String value;

    public String getStrength() {
        return strength;
    }

    public String getValue() {
        return value;
    }
}
