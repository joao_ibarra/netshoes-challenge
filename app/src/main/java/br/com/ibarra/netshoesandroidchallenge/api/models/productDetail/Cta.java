package br.com.ibarra.netshoesandroidchallenge.api.models.productDetail;

/**
 * Created by joaoibarra on 20/12/15.
 */
public class Cta {
    private String name;
    private int value;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
