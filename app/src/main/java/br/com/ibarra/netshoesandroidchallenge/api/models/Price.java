package br.com.ibarra.netshoesandroidchallenge.api.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class Price {
    @SerializedName("original_price")
    private String originalPrice;
    @SerializedName("from_price")
    private String fromPrice;
    private String saving;
    @SerializedName("payment_condition")
    private String paymentCondition;
    @SerializedName("actual_price")
    private String actualPrice;

    public String getOriginalPrice() {
        return originalPrice;
    }

    public String getFromPrice() {
        return fromPrice;
    }

    public String getSaving() {
        return saving;
    }

    public String getPaymentCondition() {
        return paymentCondition;
    }

    public String getActualPrice() {
        return actualPrice;
    }
}
