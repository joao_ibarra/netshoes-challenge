package br.com.ibarra.netshoesandroidchallenge.ui.activities;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.ibarra.netshoesandroidchallenge.R;
import br.com.ibarra.netshoesandroidchallenge.api.models.productDetail.Characteristic;
import br.com.ibarra.netshoesandroidchallenge.api.models.productDetail.ProductResult;
import br.com.ibarra.netshoesandroidchallenge.api.models.productDetail.Recommendation;
import br.com.ibarra.netshoesandroidchallenge.api.service.ApiServiceImpl;
import br.com.ibarra.netshoesandroidchallenge.ui.adapter.ProductAdapter;
import br.com.ibarra.netshoesandroidchallenge.ui.holder.RecommendationHolder;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ProductDetailActivity extends AppCompatActivity implements BaseActivity{
    @Bind(R.id.description) TextView description;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.product_image) ImageView product_image;
    @Bind(R.id.original_price) TextView original_price;
    @Bind(R.id.price) TextView price;
    @Bind(R.id.product_name) TextView product_name;
    @Bind(R.id.reload) Button reloadButton;
    @Bind(R.id.cart) FloatingActionButton cart;
    @Bind(R.id.content) RelativeLayout contentLayout;
    @Bind(R.id.progressbar) LinearLayout progressbarLayout;
    @Bind(R.id.error) RelativeLayout errorLayout;
    View view;

    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getIntent().getExtras()!=null)
            url = getIntent().getExtras().getString("URL");

        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        toolbar.setTitle(null);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getProduct();
    }

    @OnClick(R.id.cart)
    public void buy(){
        Snackbar.make(view, "Buy me S2", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @OnClick(R.id.reload)
    public void reload(){
        getProduct();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_product_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getProduct(){
        onLoadProgress();
        Call<ProductResult> call = ApiServiceImpl.getInstance().getProduct(url.replace("/produto/", ""));
        call.enqueue(new Callback<ProductResult>() {
            @Override
            public void onResponse(Response<ProductResult> response, Retrofit retrofit) {
                if(response.isSuccess()){
                    setView(response.body());
                    onFinishProgress();
                    showContent();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                t.fillInStackTrace();
                onFinishProgress();
                onFinishError();
            }
        });
    }


    private void setView(ProductResult productResult){
        LinearLayout productDetails = (LinearLayout) findViewById(R.id.product_details);
        description.setText(productResult.getValue().getDescription());
        product_name.setText(productResult.getValue().getName());
        price.setText(productResult.getValue().getPrice().getActualPrice());
        original_price.setText(productResult.getValue().getPrice().getOriginalPrice());
        original_price.setPaintFlags(original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        Picasso.with(ProductDetailActivity.this)
                .load("http:" + productResult.getValue().getGallery().get(0).getItems().get(0).getZoom())
                .into(product_image);
        for(int i =0; i < productResult.getValue().getCharacteristics().size();i++){
            productDetails.addView(getViewCharacteristic(productDetails, productResult.getValue().getCharacteristics().get(i)));
        }

        for(int i =0; i < productResult.getValue().getRecommendations().size();i++){
            productDetails.addView(getViewRecommendation(productDetails, productResult.getValue().getRecommendations().get(i)));
        }

    }

    private View getViewCharacteristic (LinearLayout parent, Characteristic characteristic){
        ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(this);
        view = inflater.inflate(R.layout.characteristic_view_holder,null);
        holder = new ViewHolder(view);
        view.setTag(holder);
        holder.titleCharacteristic.setText(characteristic.getName());
        holder.descriptionCharacteristic.setText(characteristic.getValue());
        return view;
    }

    private View getViewRecommendation (LinearLayout parent, Recommendation recommendation){
        RecommendationHolder holder;
        LayoutInflater inflater = LayoutInflater.from(this);
        view = inflater.inflate(R.layout.recommendation_holder, null);
        holder = new RecommendationHolder(view);
        view.setTag(holder);
        holder.getTitle().setText(recommendation.getLabel());

        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());

        if(recommendation.getProducts()!=null) {
            holder.getRecommendationsRecyclerView().setLayoutManager(layoutManager);
            ProductAdapter productAdapter = new ProductAdapter(recommendation.getProducts());
            holder.getRecommendationsRecyclerView().setAdapter(productAdapter);
            holder.getRecommendationsRecyclerView().setLayoutManager(new GridLayoutManager(view.getContext(), 2));
        }
        return view;
    }

    @Override
    public void onLoadProgress() {
        hideError();
        hideContent();
        progressbarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFinishProgress() {
        progressbarLayout.setVisibility(View.GONE);
    }

    @Override
    public void onFinishError() {
        hideContent();
        errorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideError() {
        errorLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideContent() {
        contentLayout.setVisibility(View.GONE);
        cart.setVisibility(View.GONE);
    }

    @Override
    public void showContent() {
        contentLayout.setVisibility(View.VISIBLE);
        cart.setVisibility(View.VISIBLE);
    }

    static class ViewHolder {
        @Bind(R.id.title_characteristic) TextView titleCharacteristic;
        @Bind(R.id.description_characteristic) TextView descriptionCharacteristic;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
