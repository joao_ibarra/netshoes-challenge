package br.com.ibarra.netshoesandroidchallenge.ui.adapter;

import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.ibarra.netshoesandroidchallenge.R;
import br.com.ibarra.netshoesandroidchallenge.api.models.Product;
import br.com.ibarra.netshoesandroidchallenge.ui.holder.ProductHolder;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductHolder> {
    private static List<Product> products;
    public ProductAdapter(List<Product> products) {
        if (this.products == null) {
            this.products = products;
        } else {
            this.products.addAll(this.products.size(),products);
        }
        notifyItemInserted(products.size());
        notifyDataSetChanged();
    }
    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.product_holder, parent, false);

        return new ProductHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, int position) {
        Product product = products.get(position);
        /*OkHttpClient okHttpClient = new OkHttpClient();
        OkHttpDownloader okHttpDownloader = new OkHttpDownloader(okHttpClient);
        Picasso picasso = new Picasso.Builder(holder.getImage().getContext()).downloader(okHttpDownloader).build();
       Picasso.with(holder.getImage().getContext())
                .load(book.getCoverImagePath())
                .resize(500, 500)
                .centerInside()
                .into(viewHolder.mCover);*/
       // Picasso picasso = new Picasso.Builder(holder.getImage().getContext()).build();
        if(product!= null && product.getImage()!=null) {
            Picasso.with(holder.getImage().getContext())
                    .load("http:" + product.getImage().getLarge())
                    .into(holder.getImage());

            holder.getName().setText(product.getName());
            if (product.getStamps() != null && product.getStamps().size() > 0)
                holder.getLabel().setText(product.getStamps().get(0).getLabel());
            holder.getOriginalPrice().setText(product.getPrice().getOriginalPrice());
            holder.getOriginalPrice().setPaintFlags(holder.getOriginalPrice().getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.getPrice().setText(product.getPrice().getActualPrice());
            holder.itemView.setTag(R.string.product_url, product.getUrl());
        }
        /*holder.itemView.setTag(R.string.tag_book_title, book.getTitle());*/
    }

    @Override
    public int getItemCount() {
        return  products.size();
    }

    public void addProduct(final Product product, final int position) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                products.add(position, product);
                notifyItemInserted(position);
            }
        }, 0);
    }

    public void remove(final int position) {
        products.remove(position);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyItemRemoved(position);
            }
        }, 0);
    }

    public void addToEnd(List<Product> products) {
        if (this.products == null) {
            this.products = products;
        } else {
            this.products.addAll(this.products.size(),products);
        }
        notifyItemInserted(products.size());
        notifyDataSetChanged();
    }

}
