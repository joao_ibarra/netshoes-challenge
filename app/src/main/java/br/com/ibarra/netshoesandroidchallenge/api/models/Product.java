package br.com.ibarra.netshoesandroidchallenge.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class Product {
    private Image image;
    private String base_sku;
    private String sku;
    private Seller seller;
    private String url;
    private Price price;
    private GtmData gtmData;
    private String sellerId;
    @SerializedName("hidden_fields")
    private List<HiddenField> hiddenFields;
    private String name;
    private List<Stamp> stamps;
    private List<Attribute> attribute;

    public Image getImage() {
        return image;
    }

    public String getBase_sku() {
        return base_sku;
    }

    public String getSku() {
        return sku;
    }

    public Seller getSeller() {
        return seller;
    }

    public String getUrl() {
        return url;
    }

    public Price getPrice() {
        return price;
    }

    public GtmData getGtmData() {
        return gtmData;
    }

    public String getSellerId() {
        return sellerId;
    }

    public List<HiddenField> getHiddenFields() {
        return hiddenFields;
    }

    public String getName() {
        return name;
    }

    public List<Stamp> getStamps() {
        return stamps;
    }

    public List<Attribute> getAttribute() {
        return attribute;
    }
}
