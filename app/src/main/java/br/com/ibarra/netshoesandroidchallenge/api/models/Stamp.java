package br.com.ibarra.netshoesandroidchallenge.api.models;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class Stamp {
    private int id;
    private String label;

    public int getId() {
        return id;
    }

    public String getLabel() {
        if(label!=null)
            return label;
        return "";
    }
}
