package br.com.ibarra.netshoesandroidchallenge.api.models;

import android.graphics.Color;

import java.util.List;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class Item {
    private String id;
    private boolean selected;
    /*private List<Color> color;*/
    private String colorPrice;
    private String name;
    private String value;
    private Image image;
    private String label;
    private boolean available;
    private String url;
}
