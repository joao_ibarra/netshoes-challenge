package br.com.ibarra.netshoesandroidchallenge.api.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import br.com.ibarra.netshoesandroidchallenge.api.deserializer.RestDeserializer;
import br.com.ibarra.netshoesandroidchallenge.api.models.Product;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class ApiServiceTestImpl {
    public static ApiService apiService;

    public static ApiService getInstance(){
        if(apiService ==null)
            return new ApiServiceTestImpl().create();

        return apiService;
    }

    private ApiServiceTestImpl(){}

    public ApiService create() {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Product.class, new RestDeserializer<>(Product.class, "result"))
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.netshoes.com.br")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        apiService = retrofit.create(ApiService.class);
        return apiService;
    }
}
