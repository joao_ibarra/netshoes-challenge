package br.com.ibarra.netshoesandroidchallenge.api.models;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class GtmData {
    private int position;
    private String category;
    private String subcategory;
    private String name;
    private String brand;
    private String list;
    private String sku;
    private String variant;
}
