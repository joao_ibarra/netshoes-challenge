package br.com.ibarra.netshoesandroidchallenge;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import br.com.ibarra.netshoesandroidchallenge.api.models.Product;
import br.com.ibarra.netshoesandroidchallenge.api.models.Result;
import br.com.ibarra.netshoesandroidchallenge.api.service.ApiServiceImpl;
import br.com.ibarra.netshoesandroidchallenge.listener.EndlessRecyclerOnScrollListener;
import br.com.ibarra.netshoesandroidchallenge.ui.activities.BaseActivity;
import br.com.ibarra.netshoesandroidchallenge.ui.adapter.ProductAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class HomeActivity extends AppCompatActivity implements BaseActivity{
    @Bind(R.id.toolbar) Toolbar toolbar ;
    @Bind(R.id.products) RecyclerView productsList ;
    @Bind(R.id.progressbar) LinearLayout progressbarLayout;
    @Bind(R.id.error) RelativeLayout errorLayout;
    @Bind(R.id.content) RelativeLayout contentLayout;
    @Bind(R.id.reload) Button reloadButton;
    private LinearLayoutManager layoutManager;
    private ProductAdapter productAdapter = new ProductAdapter(new ArrayList<Product>());
    private Map<String, String> nextUrl;
    private boolean hasNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        layoutManager = new LinearLayoutManager(this);
        productsList.setLayoutManager(layoutManager);
        search();
        productsList.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                searchInfinite();
            }
        });
        productsList.setAdapter(productAdapter);
    }

    @OnClick(R.id.reload)
    public void reload(){
        search();
    }
    public void search(){
        onLoadProgress();
        Call<Result> call = ApiServiceImpl.getInstance().productsList("OR(product.productType.displayName:Tênis,product.productType.displayName:Tênis)");
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Response<Result> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Result result = response.body();
                    try {
                        //?N=4294967024&No=60&Nr=AND%28OR%28product.productType.displayName%3AT%C3%AAnis%2Cproduct.productType.displayName%3AT%C3%AAnis%29%2Csku.siteId%3A100005%29&Nrpp=60&Ns=&Nu=sku.productIdSKUColor
                        nextUrl = urlToMap(new URL("http://www.netshoes.com.br/" + result.getValue().getUrl()));//.replace("?N=", "");
                    } catch (MalformedURLException me) {

                    } catch (UnsupportedEncodingException ue) {

                    }

                    hasNext = result.getValue().isHasNext();
                    productAdapter = new ProductAdapter(result.getValue().getProducts());
                    productsList.setAdapter(productAdapter);
                    productsList.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                    onFinishProgress();
                    showContent();
                }

            }

            @Override
            public void onFailure(Throwable t) {
                t.fillInStackTrace();
                onFinishProgress();
                onFinishError();
            }
        });
    }

    public void searchInfinite(){
        if(hasNext == true && nextUrl.size()==6) {
            Call<Result> call = ApiServiceImpl.getInstance().infiniteProductsList(
                    nextUrl.get("N"),
                    nextUrl.get("No"),
                    nextUrl.get("Nr"),
                    nextUrl.get("Nrpp"),
                    nextUrl.get("Ns"),
                    nextUrl.get("Nu"));
            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Response<Result> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        Result result = response.body();
                        productAdapter.addToEnd(result.getValue().getProducts());
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    t.fillInStackTrace();
                    Log.i("ERRO", "Má Formação, Problemas com a Serialização: Esperado Objeto, mas encontrado String");
                }
            });
        }
    }

    public static Map<String, String> urlToMap(URL url) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<>();
        String query = url.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    @Override
    public void onLoadProgress() {
        hideError();
        hideContent();
        progressbarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFinishProgress() {
        progressbarLayout.setVisibility(View.GONE);
    }

    @Override
    public void onFinishError() {
        hideContent();
        errorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideError() {
        errorLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideContent() {
        contentLayout.setVisibility(View.GONE);
    }

    @Override
    public void showContent() {
        contentLayout.setVisibility(View.VISIBLE);
    }
}
