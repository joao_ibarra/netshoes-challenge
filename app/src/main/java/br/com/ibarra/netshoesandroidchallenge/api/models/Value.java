package br.com.ibarra.netshoesandroidchallenge.api.models;

import java.util.List;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class Value {
    private int total;
    private Navigation navigation;
    private boolean hasNext;
    private List<Product> products;
    private String url;

    public int getTotal() {
        return total;
    }

    public Navigation getNavigation() {
        return navigation;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public List<Product> getProducts() {
        return products;
    }

    public String getUrl() {
        return url;
    }
}
