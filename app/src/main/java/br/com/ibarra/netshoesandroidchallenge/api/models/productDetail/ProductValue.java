package br.com.ibarra.netshoesandroidchallenge.api.models.productDetail;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.ibarra.netshoesandroidchallenge.api.models.Attribute;
import br.com.ibarra.netshoesandroidchallenge.api.models.HiddenField;
import br.com.ibarra.netshoesandroidchallenge.api.models.Navigation;
import br.com.ibarra.netshoesandroidchallenge.api.models.Price;
import br.com.ibarra.netshoesandroidchallenge.api.models.Product;
import br.com.ibarra.netshoesandroidchallenge.api.models.Seller;
import br.com.ibarra.netshoesandroidchallenge.api.models.Stamp;

/**
 * Created by joaoibarra on 20/12/15.
 */
public class ProductValue {
//    EStes campos não estou serializando por não saber o que esperar neles
//    "superDiscountEndDate" -> "null", "remaining" -> """", "banner" -> "null", "download" -> "null","rating" -> "null"
//    "personalization" -> """"

    private  boolean isPresale;
    private  boolean liquidPixelAvailability;
    @SerializedName("base_sku")
    private  String baseSku;
    private  boolean oneclick;
    private  String id;
    private  boolean hideShippingMessage;
    private  String shipping;
    private  String description;
    private  String name;
    private  String hostName;
    private List<Stamp> stamps;
    private String shippingMessage;
    private String sku;
    private Seller seller;
    private Cta cta;
    private String url;
    private Price price;
    @SerializedName("hidden_fields")
    private List<HiddenField> hiddenFields;
    @SerializedName("multiplus_points")
    private int multiplusPoints;
    private Badge badge;
    private List<Attribute> attributes;
    private List<Recommendation> recommendations;
    private String similarProductsSearch;
    private List<Characteristic> characteristics;
    private List<Gallery> gallery;

    public boolean isPresale() {
        return isPresale;
    }

    public boolean isLiquidPixelAvailability() {
        return liquidPixelAvailability;
    }

    public String getBaseSku() {
        return baseSku;
    }

    public boolean isOneclick() {
        return oneclick;
    }

    public String getId() {
        return id;
    }

    public boolean isHideShippingMessage() {
        return hideShippingMessage;
    }

    public String getShipping() {
        return shipping;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getHostName() {
        return hostName;
    }

    public List<Stamp> getStamps() {
        return stamps;
    }

    public String getShippingMessage() {
        return shippingMessage;
    }

    public String getSku() {
        return sku;
    }

    public Seller getSeller() {
        return seller;
    }

    public Cta getCta() {
        return cta;
    }

    public String getUrl() {
        return url;
    }

    public Price getPrice() {
        return price;
    }

    public List<HiddenField> getHiddenFields() {
        return hiddenFields;
    }

    public int getMultiplusPoints() {
        return multiplusPoints;
    }

    public Badge getBadge() {
        return badge;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public List<Recommendation> getRecommendations() {
        return recommendations;
    }

    public String getSimilarProductsSearch() {
        return similarProductsSearch;
    }

    public List<Characteristic> getCharacteristics() {
        return characteristics;
    }

    public List<Gallery> getGallery() {
        return gallery;
    }
}
