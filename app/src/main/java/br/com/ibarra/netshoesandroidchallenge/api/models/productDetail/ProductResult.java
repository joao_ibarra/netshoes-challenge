package br.com.ibarra.netshoesandroidchallenge.api.models.productDetail;

/**
 * Created by joaoibarra on 20/12/15.
 */
public class ProductResult {
    private String message;
    private String exception;
    private ProductValue value;
    private boolean isSuccess;

    public String getMessage() {
        return message;
    }

    public String getException() {
        return exception;
    }

    public ProductValue getValue() {
        return value;
    }

    public boolean isSuccess() {
        return isSuccess;
    }
}
