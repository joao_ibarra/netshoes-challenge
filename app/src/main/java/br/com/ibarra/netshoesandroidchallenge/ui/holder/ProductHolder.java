package br.com.ibarra.netshoesandroidchallenge.ui.holder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.ibarra.netshoesandroidchallenge.R;
import br.com.ibarra.netshoesandroidchallenge.ui.activities.ProductDetailActivity;
import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by joaoibarra on 19/12/15.
 */

public class ProductHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener{
    @Bind(R.id.name) TextView name;
    @Bind(R.id.price) TextView price;
    @Bind(R.id.original_price) TextView originalPrice;
    @Bind(R.id.label) TextView label;
    @Bind(R.id.image) ImageView image;

    public ProductHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        Intent intent = new Intent(view.getContext(), ProductDetailActivity.class);
        intent.putExtra("URL", view.getTag(R.string.product_url).toString());
        view.getContext().startActivity(intent);
       /* EventBus.getDefault().post(new BookSelectedEvent(
                (Long)view.getTag(R.string.tag_book_id),
                (String)view.getTag(R.string.tag_book_title)));*/
    }

    public TextView getName() {
        return name;
    }

    public TextView getPrice() {
        return price;
    }

    public TextView getOriginalPrice() {
        return originalPrice;
    }

    public TextView getLabel() {
        return label;
    }

    public ImageView getImage() {
        return image;
    }
}
