package br.com.ibarra.netshoesandroidchallenge.api.models.productDetail;

import java.util.List;

import br.com.ibarra.netshoesandroidchallenge.api.models.Product;

/**
 * Created by joaoibarra on 20/12/15.
 */
public class Recommendation {
    private String label;
    private List<Product> products;

    public String getLabel() {
        return label;
    }

    public List<Product> getProducts() {
        return products;
    }
}
