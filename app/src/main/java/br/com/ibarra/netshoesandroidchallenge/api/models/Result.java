package br.com.ibarra.netshoesandroidchallenge.api.models;

/**
 * Created by joaoibarra on 19/12/15.
 */
public class Result {
    private String message;
    private String exception;
    private Value value;
    private boolean isSuccess;

    public String getMessage() {
        return message;
    }

    public String getException() {
        return exception;
    }

    public Value getValue() {
        return value;
    }

    public boolean isSuccess() {
        return isSuccess;
    }
}
