package br.com.ibarra.netshoesandroidchallenge.api.models.productDetail;

/**
 * Created by joaoibarra on 20/12/15.
 */
public class Characteristic {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
