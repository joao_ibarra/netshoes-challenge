package br.com.ibarra.netshoesandroidchallenge.api.service;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import br.com.ibarra.netshoesandroidchallenge.api.models.Result;
import br.com.ibarra.netshoesandroidchallenge.api.models.Value;
import br.com.ibarra.netshoesandroidchallenge.api.models.productDetail.ProductResult;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by joaoibarra on 19/12/15.
 */
public interface ApiService {

//    @Headers("Accept: Application/JSON")
    @Headers({
            "Accept: Application/JSON",
            "User-Agent: Netshoes App "})
    @GET("/departamento")
    Call<Result> productsList(@Query("Nr") String query);

    @Headers({
            "Accept: Application/JSON",
            "User-Agent: Netshoes App "})
    @GET("/departamento")
    Call<Result> infiniteProductsList(@Query("N") String n,
                                      @Query("No") String no,
                                      @Query("Nr") String nr,
                                      @Query("Nrpp") String nrpp,
                                      @Query("Ns") String ns,
                                      @Query("Nu") String nu);

    @Headers({
            "Accept: Application/JSON",
            "User-Agent: Netshoes App "})
    @GET("/produto/{url}")
    Call<ProductResult> getProduct(@Path("url") String url);
}
